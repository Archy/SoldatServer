#pragma once
#include "MenuState.h"
#include "GameStateCreator.h"


class MainMenuState:
	public MenuState
{
public:
	virtual ~MainMenuState();

	virtual bool EnterState();
	virtual bool ExitState();

private:
	friend class MainMenuStateCreator;
	MainMenuState(int stateID);

	//add callbacks functions to callbacks vector
	virtual void pushCallbacks();

	//static callback functions
	static void exitFunction();
	static void playFunction();
};


class MainMenuStateCreator : public GameStateCreator
{
	virtual GameState* createGameObject(const int id) const
	{
		return new MainMenuState(id);
	}
};