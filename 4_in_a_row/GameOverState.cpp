#include "GameOverState.h"
#include "Game.h"

#include "LoaderParams.h"
#include "AnimatedBackground.h"
#include "MenuButton.h"
#include "AnimatedObject.h"

GameOverState::GameOverState(int stateID) :
	MenuState(stateID)
{
}


GameOverState::~GameOverState()
{
}


bool GameOverState::EnterState()
{
	// TEXTURES
	SDL_Color transparent{ 255, 255, 255, 255 };

	if (Game::Instance()->getTextureMenager()->loadTexture(
		"assets/menubutton.png", "button",
		Game::Instance()->getRenderer(), &transparent))
	{
		mTextureIDList.push_back("button"); // push into list
	}
	if (Game::Instance()->getTextureMenager()->loadTexture(
		"assets/clouds2.png", "cloud2",
		Game::Instance()->getRenderer()))
	{
		mTextureIDList.push_back("cloud2"); // push into list
	}

	if (Game::Instance()->getTextureMenager()->loadSpriteShit(
		"assets/gameover.png", "gameover",
		Game::Instance()->getRenderer(), NULL, "assets/gameover.txt"))
	{
		mTextureIDList.push_back("gameover"); // push into list
	}

	// TEXTTEXTURES
	SDL_Color textColor{ 255, 255, 0 ,255 };
	if (Game::Instance()->getTextureMenager()->createTextTexture(
		"Restart", textColor, Game::Instance()->getRenderer()))
	{
		mTextureIDList.push_back("Restart"); // push into list
	}

	textColor = SDL_Color{ 0, 255, 0 ,255 };
	if (Game::Instance()->getTextureMenager()->createTextTexture(
		"Exit", textColor, Game::Instance()->getRenderer()))
	{
		mTextureIDList.push_back("Exit"); // push into list
	}

	// OBJECTS
	std::shared_ptr<AnimatedBackground> background = std::make_shared<AnimatedBackground>();
	background->load(std::unique_ptr<LoaderParams>(new LoaderParams(0, 0,
		Game::Instance()->getGameWidth(), Game::Instance()->getGameHight(), "cloud2", 1, 30)));
	mStateObjects.push_back(background);

	std::shared_ptr<MenuButton> button1 = std::make_shared<MenuButton>();
	button1->load(std::unique_ptr<LoaderParams>(new LoaderParams(100, 150,
		400, 100, "button", "Restart", 0)));
	mStateObjects.push_back(button1);

	std::shared_ptr<MenuButton> button2 = std::make_shared<MenuButton>();
	button2->load(std::unique_ptr<LoaderParams>(new LoaderParams(100, 350,
		400, 100, "button", "Exit", 1)));
	mStateObjects.push_back(button2);

	std::shared_ptr<AnimatedObject> gameOver1 = std::make_shared<AnimatedObject>();
	gameOver1->load(std::unique_ptr<LoaderParams>(new LoaderParams(130, 90,
		190, 30, "gameover", 2, 3)));
	mStateObjects.push_back(gameOver1);

	std::shared_ptr<AnimatedObject> gameOver2 = std::make_shared<AnimatedObject>();
	gameOver2->load(std::unique_ptr<LoaderParams>(new LoaderParams(320, 90,
		190, 30, "gameover", 2, 3)));
	mStateObjects.push_back(gameOver2);



	std::cout << "entering GameOverState\n";

	pushCallbacks();
	assignCallbacks();

	return true;
}


bool GameOverState::ExitState()
{
	std::cout << "exiting GameOverState\n";
	return true;
}


void GameOverState::pushCallbacks()
{
	mCallbacks.push_back(GameOverState::restartFunction);
	mCallbacks.push_back(GameOverState::exitFunction);
}


void GameOverState::restartFunction()
{
	std::cout << "restart Function called\n";
	Game::Instance()->getStateMachine()->changeState(StateMachine::PlayState);
}

void GameOverState::exitFunction()
{
	Game::Instance()->quit();
	std::cout << "exit Function called\n";
}