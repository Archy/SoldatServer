#pragma once
#include "GameState.h"

//pure virtual class - base of every menu-like state
class MenuState :
	public GameState
{
public:
	MenuState(int stateID);
	~MenuState();

	virtual void update();
	virtual void render();

	//clear objects
	virtual void clear();
	//clear textures and sounds
	virtual void clearData();

protected:
	//assign callback functions to buttons
	void assignCallbacks();
	//add callbacks functions to callbacks vector
	virtual void pushCallbacks() = 0;

protected:
	//vector of pointer to callback functions for buttons
	std::vector<void(*)()> mCallbacks;
};



