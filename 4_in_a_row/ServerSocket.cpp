#include "ServerSocket.h"
#include <iostream>


ServerSocket::ServerSocket()
{
}


ServerSocket::~ServerSocket()
{
}

bool ServerSocket::init(int port)
{
	mPort = port;
	bool result;
	result = Socket::init(port);
	if (!result)
		return result;
	result = createServer();
	if (!result)
		Socket::close();

	return result;
}

bool ServerSocket::sendData(const char * data, int & size, std::string remoteIP, const USHORT port)
{
	int result;
	char reveiverIP[sIP_SIZE];
	strcpy_s(reveiverIP, remoteIP.c_str());

	//set clients info
	IN_ADDR addr;
	memset(&addr, 0, sizeof(addr));
	inet_pton(AF_INET, reveiverIP, &addr);

	mRemoteAddr.sin_addr.S_un.S_addr = addr.S_un.S_addr;
	mRemoteAddr.sin_port = port;

	result = sendto(mSock, data, size, 0, (SOCKADDR *)&mRemoteAddr, sizeof(mRemoteAddr));
	if (result == SOCKET_ERROR)
	{
		std::cout << "SENDING ERROR:\t";
		std::cout << WSAGetLastError();
	}
	return result != SOCKET_ERROR;
}

bool ServerSocket::createServer()
{
	//listen to all addresses
	mLocalAddr.sin_addr.s_addr = htonl(INADDR_ANY);

	// bind socket
	return (bind(mSock, (SOCKADDR *)&mLocalAddr, sizeof(mLocalAddr)) != SOCKET_ERROR);
}
