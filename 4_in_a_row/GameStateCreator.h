#pragma once

class GameState;

//pure virtual class - base of every GameStateCreator
class GameStateCreator
{
public:
	virtual GameState* createGameObject(const int id) const = 0;
	virtual ~GameStateCreator() {}
};

