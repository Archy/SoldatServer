#include "Texture.h"


Texture::Texture() :
	mHeight(0), mWidth(0)
{
}


Texture::~Texture()
{
	SDL_DestroyTexture(mTexture);
}


void Texture::load(SDL_Texture* texture, 
	int width, int height)
{
	mTexture = texture;
	mHeight = height;
	mWidth = width;
}

void Texture::load(SDL_Texture * texture, int width, int height,
		std::vector<std::vector<SDL_Rect>> frameData)
{
	mFrameData = frameData;
	load(texture, width, height);
}

void Texture::clean()
{
}


void Texture::setBlendMode(SDL_BlendMode blending)
{
	//Set blending function
	SDL_SetTextureBlendMode(mTexture, blending);
}

void Texture::setAlpha(Uint8 alpha)
{
	//Modulate texture alpha
	SDL_SetTextureAlphaMod(mTexture, alpha);
}


std::pair<int, int> Texture::getDimensions()
{
	std::pair<int, int>dimPair;
	dimPair.first = mWidth;
	dimPair.second = mHeight;

	return dimPair;
}