#include <cstdlib>
#include <iostream>
#include <SDL.h>
#include "Game.h"



int main(int argc, char* argv[])
{
	//init attempt
	//name, window x/y position, width, height, fullscreen
	if (Game::Instance()->init("Soldat", 100, 100, 640, 480,
		false))
	{
		std::cout << "game init success!" << '\n';
		
		//main game loop
		while (Game::Instance()->isRunning())
		{
			//handle users input
			Game::Instance()->handleEvents();
			//update game state
			Game::Instance()->update();
			//draw game
			Game::Instance()->render();
			//print game logs to file and console
			
		}
	}
	else	//init failure
	{
		std::cout <<"game init failure - " << SDL_GetError() << "\n";
		
		return EXIT_FAILURE;
	}
	//free resources
	Game::Instance()->clean();
	Game::Instance()->deleteInstance();
	//print game log
	
	return EXIT_SUCCESS;
}