#pragma once
#include <map>

class GameStateCreator;
class GameState;
class StateMachine;

class GameStateFactory
{
public:
	~GameStateFactory();

	//add state to map
	void registerState(int id, GameStateCreator* stateCreator);

	GameState* createState(const int id);

	void clean();

private:
	//only StateMachine instance can create GameStateFactory
	friend class StateMachine;

	GameStateFactory();

private:
	std::map<int, GameStateCreator*> mStatesCreator;

};