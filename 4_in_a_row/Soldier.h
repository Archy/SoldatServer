#pragma once
#include "AnimatedObject.h"
#include "Board.h"
#include "BulletHandler.h"
#include <memory>

class Soldier :
	public AnimatedObject
{
public:
	enum SoldierState {
		MOVE,
		STAY,
		DIE
	};

	enum SoldierAction {
		JUMP,
		FALL,
		SHOOT,
		NONE
	};

	enum SoldierDir {
		LEFT,
		RIGHT
	};

public:
	Soldier(int nr);
	~Soldier();

	virtual void update();

	void setBoard(std::shared_ptr<Board> board) { mBoard = board; }
	void setBulletHandler(std::shared_ptr<BulletHandler> bulletHandler) 
		{ mBulletHandler = bulletHandler; }
	void setState(SoldierState state, SoldierAction action, SoldierDir dir);
	
	SoldierDir getDir() const { return mDir; }
	SoldierAction getAction() const { return mAction; }
	SoldierState getState() const { return mState; }
	char getNr() const { return mNumber; }

	char getStateToSend();
	char getPlayerInfo();

	float getX() { return (float)mPosition.getX(); }
	float getY() { return (float)mPosition.getY(); }
	float getWidth() { return (float)mWidth; }
	float getHeight() { return (float)mHeight; }

	bool isAlive() { return mLives > 0; }

	void hit();

private:
	void setFrames();
	std::pair<int, int> xyToBord(float x, float y);

private:
	const char mNumber;

	SoldierState mState;
	SoldierAction mAction;
	SoldierDir mDir;

	Vector2D mSpeed;

	int mLives;
	int mAmmo;
	double mReloadTime;

	bool mCanJump;
	bool mAnimationReset;
	bool mShooted;

	std::shared_ptr<Board> mBoard;
	std::shared_ptr<BulletHandler> mBulletHandler;

private:
	static const int MOVEMENT_SPEED = 50;
	static const int JUMP_ACCELERAION = -1000;
	static const int GRAVITY = 100;

	static const double RELOAD_TIME;
	static const int MAX_AMMO = 3;
};

