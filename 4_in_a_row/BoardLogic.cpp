#include "BoardLogic.h"

#include "GameLog.h"

BoardLogic::BoardLogic(const BoardLogic* oldBoard) :
	m_bordWidth(7), m_boardHeight(7)
{
	this->m_tab = new Board::field*[this->m_boardHeight];
	for (int h = 0; h < this->m_boardHeight; ++h)
	{
		this->m_tab[h] = new Board::field[this->m_bordWidth];
		for (int w = 0; w < this->m_bordWidth; ++w)
		{
			if (oldBoard != nullptr)
			{
				(this->m_tab)[h][w] = (*oldBoard)[h][w];
			}
			else
			{
				this->m_tab[h][w] = Board::field::empty;
			}
		}
	}
}


BoardLogic::~BoardLogic()
{
	for (int h = 0; h < this->m_boardHeight; ++h)
	{
		delete[] this->m_tab[h];
	}
	delete[] this->m_tab;
}


void BoardLogic::operator=(const BoardLogic* oldBoard)
{
	//GameLog::Instance() << "Board logic operator =";

	for (int h = 0; h < this->m_boardHeight; ++h)
	{
		this->m_tab[h] = new Board::field[this->m_bordWidth];
		for (int w = 0; w < this->m_bordWidth; ++w)
		{
			if (oldBoard != nullptr)
			{
				(this->m_tab)[h][w] = (*oldBoard)[h][w];
			}
			else
			{
				this->m_tab[h][w] = Board::field::empty;
			}
		}
	}
}


Board::field* BoardLogic::operator[](unsigned int row) const
{
	return (this->m_tab[row]);
}


bool BoardLogic::throwCoint(int column, Board::field player)
{
	//column is full
	if (this->m_tab[0][column] != Board::field::empty)
	{
		return false;
	}

	for (int i = this->m_boardHeight - 1; i >= 0; --i)
	{
		if (this->m_tab[i][column] == Board::field::empty)
		{
			this->m_tab[i][column] = player;
			return true;
		}
	}

	GameLog::Instance() << "Cant throw coin in: " << column <<
		"but column is not full\n";
	return false;
}

//chujowo i nie dziala
//skosy tylko najwyzszych w kolumnie
bool BoardLogic::checkVictory(Board::field player) const
{
	bool victory = false;
	int counter = 0;
	int counter2 = 0;

	//vertical
	for (int x = 0; x < this->m_bordWidth; ++x)
	{
		counter = 0;
		for (int y = this->m_boardHeight - 1; y >= 0; --y)
		{
			if (this->m_tab[y][x] == player)
			{
				++counter;
				if (counter == 4)
				{
					victory = true;
					GameLog::Instance() << "Game ended 4 in column: " <<
						x << "\n";
					break;
				}
			}
			else if (this->m_tab[y][x] == Board::field::empty)
			{
				break;
			}
			else
			{
				counter = 0;
			}
		}

		if (victory)
			break;
	}

	//horizontal
	if (!victory)
	{
		for (int y = this->m_boardHeight - 1; y >= 0; --y)
		{
			counter = 0;
			for (int x = 0; x < this->m_bordWidth; ++x)
			{
				if (this->m_tab[y][x] == player)
				{
					++counter;
					if (counter == 4)
					{
						victory = true;
						GameLog::Instance() << "Game ended 4 in column: " <<
							x << "\n";
						break;
					}
				}
				else
				{
					counter = 0;
				}
			}

			if (victory)
				break;
		}
	}

	//askew left-up to right-down
	if (!victory)
	{
		for (int i = 3; i >= 0; --i)
		{
			for (int j = i, k = 0; j < this->m_bordWidth; ++j, ++k)
			{
				if (this->m_tab[j][k] == player)
				{
					++counter;
					if (counter == 4)
					{
						victory = true;
						GameLog::Instance() << "Game ended 4 in column: " <<
							j << "\n";
						break;
					}
				}
				else
				{
					counter = 0;
				}

				//reversed coordinates
				if (this->m_tab[k][j] == player)
				{
					++counter2;
					if (counter2 == 4)
					{
						victory = true;
						GameLog::Instance() << "Game ended 4 in column: " <<
							j << "\n";
						break;
					}
				}
				else
				{
					counter2 = 0;
				}
			}

			if (victory)
				break;
		}
	}

	//askew left-down to right-up
	if (!victory)
	{
		for (int i = 3; i < this->m_bordWidth; ++i)
		{
			for (int j = i, k1 = 0, k2 = 6; j >= 0; --j, ++k1, --k2)
			{
				if (this->m_tab[j][k1] == player)
				{
					++counter;
					if (counter == 4)
					{
						victory = true;
						GameLog::Instance() << "Game ended 4 in column: " <<
							j << "\n";
						break;
					}
				}
				else
				{
					counter = 0;
				}

				//reversed coordinates
				if (this->m_tab[k2][6 - j] == player)
				{
					++counter2;
					if (counter2 == 4)
					{
						victory = true;
						GameLog::Instance() << "Game ended 4 in column: " <<
							j << "\n";
						break;
					}
				}
				else
				{
					counter2 = 0;
				}
			}
			if (victory)
				break;
		}
	}

	return victory;
}