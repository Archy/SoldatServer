#pragma once
#include "GameObject.h"
#include <SDL.h>

//base of every animated object in game
class AnimatedObject :
	public GameObject
{
public:
	AnimatedObject();
	~AnimatedObject();

	virtual void load(std::unique_ptr<LoaderParams> const &pParams);

	virtual void draw();
	virtual void update();
	virtual void clean();

protected:
	//animation data
	int mCurrentFrame;
	int mFramesNumber;
	int mAnimationRow;
	int mAnimSpeed;

	SDL_RendererFlip mFlip;
	bool mLoopAnim;
	bool mAnimFinished;

private:
	float mFrameTime;
};

