#pragma once

//used for packets numeration
class LoopedCounter
{
public:
	LoopedCounter();
	~LoopedCounter();

	bool operator<(const unsigned char& other);
	
	unsigned char getNext();
	void setValue(unsigned char x);
private:
	unsigned char mx;

};

