#pragma once
#include "AnimatedObject.h"
#include "Vector2D.h"
#include <vector>
#include "Board.h"
#include <SDL.h>

class BulletHandler :
	public AnimatedObject
{
private:
	struct Bullet
	{
		Vector2D pos;
		SDL_RendererFlip flip;
		bool remove;

		Bullet(float x, float y, SDL_RendererFlip pFlip) : 
			pos(x, y), remove(false), flip(pFlip)
		{
		}
	};

public:
	BulletHandler();
	~BulletHandler();

	virtual void draw();
	virtual void update();

	void addBullet(float x, float y, SDL_RendererFlip flip);

	void getDataToSend(char *tab, int &size);

	void setBoard(std::shared_ptr<Board> board) { mBoard = board; }

	bool playerColision(float x, float y, float w, float h);

private:
	std::pair<int, int> xyToBord(float x, float y);

private:
	std::vector<Bullet> mBullets;

	static const int sBulletWidth = 16;
	static const int sBulletHeight = 8;

	const Vector2D BULLET_SPEED;

	std::shared_ptr<const Board> mBoard;
};

