#pragma once
#include <vector>
#include <memory>

class GameState;
class GameStateFactory;

class StateMachine
{
public:
	enum GameStatesIDs
	{
		PlayState,
		MainMenuState,
		ExitState,
		GameEndState
	};

public:
	StateMachine();
	~StateMachine();

	void init();

	//add new state
	void addState(const GameStatesIDs id);
	//remove state by id
	void removeState(const GameStatesIDs id);
	//remove state is currently being used
	void removeCurrentState();
	//change state that is currently used
	void changeState(const GameStatesIDs id);

	void update();
	void render();

	void clean();

private:
	void removeOldStates();

	void findState(GameState** stateToRemove, int* statePos, int id);

private:
	//vector containing game states
	std::vector<GameState*> mGameState;

	//for deleting unused states
	std::vector<GameState*> mOldGameStates;

	//game states factory
	std::unique_ptr<GameStateFactory>mStatesFactory;

};

