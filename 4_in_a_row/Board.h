#pragma once
#include "GameObject.h"
#include <string>

class Board :
	public GameObject
{
public:
	Board();
	~Board();

	virtual void draw();
	virtual void update();
	virtual void clean();

	bool getBrick(int x, int y) const;
	int getRows() const { return sHEIGHT; }
	int getCol() const { return sWIDTH; }
	int getDY() const { return static_cast<int>(mPosition.getY()); }
	int getBrickW() const;
	int getBrickH() const;

private:
	void init();

private:
	constexpr static const int sWIDTH = 16;
	constexpr static const int sHEIGHT = 19;
	bool mTab[sHEIGHT][sWIDTH];
};

