#pragma once
#include <string>
#include <SDL.h>
#include <SDL_ttf.h>
#include <memory>

class Texture;

class TextTextureManager
{
public:
	TextTextureManager();
	~TextTextureManager();

	bool loadFont(std::string fontFile, int size);
	void clear();

	//create Texture instance with text texure loaded
	std::unique_ptr<Texture> createTextTexture(std::string textureText, SDL_Color textColor,
		SDL_Renderer* pRenderer);

private:
	std::shared_ptr<TTF_Font> mFont;
};

