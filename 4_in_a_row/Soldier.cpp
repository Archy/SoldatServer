#include "Soldier.h"
#include <SDL.h>
#include "Game.h"

const double Soldier::RELOAD_TIME = 2.5;

Soldier::Soldier(int nr) :
	mState(SoldierState::STAY), mAction(SoldierAction::NONE),
	mDir(SoldierDir::RIGHT), mSpeed(0,0), mCanJump(true), 
	mNumber(nr), mAnimationReset(false), mShooted(false),
	mLives(2), mAmmo(MAX_AMMO), mReloadTime(0.0)
{
	setFrames();
	if (mNumber > 3)
	{
		std::cout << "WRONG SOLDIER NUMBER: " << mNumber << "\n";
	}
}


Soldier::~Soldier()
{
}

void Soldier::update()
{
	//if soldier is dead
	if (mLives <= 0)
	{
		//get player of the screen
		if (mAnimFinished)
		{
			mPosition.setX(1000);
			mPosition.setY(1000);
		}
		AnimatedObject::update();

		return;
	}


	mSpeed.setX(0.0f);
	std::pair<int, int>  boardPos1 = xyToBord(mPosition.getX() + 2, mPosition.getY() + mHeight);
	std::pair<int, int> boardPos2 = xyToBord(mPosition.getX() + mWidth - 2, mPosition.getY() + mHeight);
	std::pair<int, int> boardPos3;

//if player is shooting he cannot move or jump
	if (mAction == Soldier::SHOOT)
	{
		if (mAnimFinished)
		{
			mAction = Soldier::NONE;
			setFrames();
			mShooted = false;
		}
		else
		{
			if (mCurrentFrame == 2 && !mShooted)
			{
				mShooted = true;
				//create bullet
				float y = mPosition.getY() + 10;
				float x = mPosition.getX() + (mFlip == SDL_FLIP_NONE ? mWidth + 1 : -17);
				mBulletHandler->addBullet(x, y, mFlip);

				--mAmmo;
			}

			AnimatedObject::update();
			return;
		}
	}

//reloading
	if (mAmmo <= 0)
	{
		mReloadTime += Game::Instance()->getDT();
		if (mReloadTime >= RELOAD_TIME)
		{
			mReloadTime = 0.0;
			mAmmo = MAX_AMMO;
		}
	}

//gravity
	//add only when nothing is below soldier
	if (!mBoard->getBrick(boardPos1.first, boardPos1.second) &&
		!mBoard->getBrick(boardPos2.first, boardPos2.second))
		mSpeed += Vector2D(0, (float)GRAVITY * Game::Instance()->getDT());

//speed and other actions
	switch (mAction)
	{
	case Soldier::JUMP:
		if (mCanJump)
		{
			mSpeed.setY(-150.0f);
			mCanJump = false;
		}
		break;
	case Soldier::NONE:
	default:
		break;
	}

	switch (mState)
	{
	case Soldier::MOVE:
		mSpeed.setX((float)MOVEMENT_SPEED*(mDir == LEFT ? -1 : 1));
		break;
	case Soldier::STAY:
		break;
	case Soldier::DIE:
		break;
	default:
		break;
	}

	mPosition += mSpeed * Game::Instance()->getDT();

//x axis window collison
	if (mPosition.getX() < 0 )
		mPosition.setX(0);
	else if (Game::Instance()->getGameWidth() < (mPosition.getX() + mWidth))
	{
		mPosition.setX((float)Game::Instance()->getGameWidth() - mWidth);
	}

//y axis window collison
	if (Game::Instance()->getGameHight() < (mPosition.getY() + mHeight))
	{
		mPosition.setY((float)Game::Instance()->getGameHight() - mHeight);
		mSpeed.setY(0);
		mCanJump = true;
		if (mAction == SoldierAction::JUMP)
		{
			mAction = SoldierAction::NONE;
			//TODO remove from server 
			setFrames();
			mAnimationReset = true;
		}
	}
	else if (mPosition.getY() < 0)
	{
		mPosition.setY(0.0f);
		mSpeed.setY(0);
	}

//board collision
	const float DX = 0.01f;
	//vertical collision
	boardPos1 = xyToBord(mPosition.getX() + 2, mPosition.getY() + mHeight);
	boardPos2 = xyToBord(mPosition.getX() + mWidth - 2, mPosition.getY() + mHeight);

	if (mBoard->getBrick(boardPos1.first, boardPos1.second) ||
		mBoard->getBrick(boardPos2.first, boardPos2.second))
	{
		mPosition.setY(static_cast<float>(boardPos1.second*mBoard->getBrickH() + mBoard->getDY() - mHeight));
		mSpeed.setY(0);
		mCanJump = true;
		if (mAction != SoldierAction::NONE)
		{
			mAction = SoldierAction::NONE;
			//TODO remove from server 
			setFrames();
			mAnimationReset = true;
		}

		boardPos1 =
			xyToBord(mPosition.getX() + 2, mPosition.getY() - 1);
		boardPos2 =
			xyToBord(mPosition.getX() + mWidth - 2, mPosition.getY() - 1);
		if (mBoard->getBrick(boardPos1.first, boardPos1.second) ||
			mBoard->getBrick(boardPos2.first, boardPos2.second) || mPosition.getY() < 6.0f)
		{
			if (mCanJump)
				mCanJump = false;
		}
	}
	else
	{
		boardPos1 =
			xyToBord(mPosition.getX() + 2, mPosition.getY());
		boardPos2 =
			xyToBord(mPosition.getX() + mWidth - 2, mPosition.getY());
		if (mBoard->getBrick(boardPos1.first, boardPos1.second) ||
			mBoard->getBrick(boardPos2.first, boardPos2.second))
		{
			mPosition.setY(static_cast<float>(boardPos1.second*mBoard->getBrickH() + mBoard->getDY() + mBoard->getBrickH()));
			mSpeed.setY(0);

			mAction = Soldier::NONE;
			setFrames();
		}

		//there is nothing below soldier -> he is falling
		if (mAction != SoldierAction::JUMP ||
			(mAction == SoldierAction::JUMP && mAnimFinished))
		{
			mAction = FALL;
			mCanJump = false;
			//TODO remove from server 
			setFrames();
		}
	}

//horizontal collsion
	boardPos1 =
		xyToBord(mPosition.getX(), mPosition.getY()+1);
	boardPos2 =
		xyToBord(mPosition.getX(), mPosition.getY() + mHeight/2);
	boardPos3 =
		xyToBord(mPosition.getX(), mPosition.getY() + mHeight-1);
	if (mBoard->getBrick(boardPos1.first, boardPos1.second) ||
		mBoard->getBrick(boardPos2.first, boardPos2.second) ||
		mBoard->getBrick(boardPos3.first, boardPos3.second))
	{
		mPosition.setX(static_cast<float>((boardPos1.first+1)*mBoard->getBrickW()+ DX));
	}
	else
	{
		boardPos1 =
			xyToBord(mPosition.getX() + mWidth, mPosition.getY()+1);
		boardPos2 =
			xyToBord(mPosition.getX() + mWidth, mPosition.getY() + mHeight / 2);
		boardPos3 =
			xyToBord(mPosition.getX() + mWidth, mPosition.getY() + mHeight-1);

		if (mBoard->getBrick(boardPos1.first, boardPos1.second) ||
			mBoard->getBrick(boardPos2.first, boardPos2.second) ||
			mBoard->getBrick(boardPos3.first, boardPos3.second))
		{
			mPosition.setX(static_cast<float>(boardPos1.first*mBoard->getBrickW() - mWidth- DX));
		}
	}

//animation
	AnimatedObject::update();
}

void Soldier::setState(SoldierState state, SoldierAction action, SoldierDir dir)
{
	if (mLives <= 0)
	{
		return;
	}


	if (mDir != dir)
	{
		mDir = dir;
		switch (mDir)
		{
		case Soldier::LEFT:
			mFlip = SDL_FLIP_HORIZONTAL;
			break;
		case Soldier::RIGHT:
			mFlip = SDL_FLIP_NONE;
			break;
		default:
			break;
		}
	}

	if (state != mState || action != mAction)
	{
		if (action == SoldierAction::SHOOT && mAction == SoldierAction::NONE && mAmmo>0)
		{
			mAction = action;
			setFrames();
		}
		else if (action == SoldierAction::JUMP && SoldierAction::NONE && mCanJump)
		{
			mState = state;
			mAction = action;
			setFrames();
		}
		else
		{
			if (mState != state)
			{
				mState = state;
				setFrames();
			}
		}
	}
}

char Soldier::getStateToSend()
{
	//prepare data to send to players

	char a = 0;

	a |= mNumber;
	a = a << 6;

	if (mDir == Soldier::SoldierDir::RIGHT)
		a |= 0x20;

	switch (mState)
	{
	case Soldier::MOVE:
		a |= 0x08;
		break;
	case Soldier::STAY:
		break;
	case Soldier::DIE:
		a |= 0x18;
		break;
	default:
		break;
	}

	switch (mAction)
	{
	case Soldier::JUMP:
		a |= 0x02;
		break;
	case Soldier::FALL:
		a |= 0x04;
		break;
	case Soldier::NONE:
		break;
	case Soldier::SHOOT:
		a |= 0x06;
		break;
	default:
		break;
	}

	if (mAnimationReset)
		a |= 0x01;

	return a;
}

char Soldier::getPlayerInfo()
{
	//	xx		xxx		xxx
	//	nr		ammo	lives

	char a = 0;

	a |= mNumber;
	a = a << 6;

	char ammo = static_cast<int>(mAmmo);
	ammo <<= 3;
	ammo &= 0x38;
	a |= ammo;

	char lives = static_cast<int>(mLives);
	lives &= 0x07;
	a |= lives;

	return a;
}

void Soldier::hit()
{
	--mLives;
	if (mLives <= 0)
	{
		mAction = Soldier::NONE;
		mState = Soldier::DIE;
		setFrames();
	}
}

void Soldier::setFrames()
{
	//start new soldiers animation

	mAnimFinished = false;
	mLoopAnim = true;

	switch (mState)
	{
	case Soldier::MOVE:
		mFramesNumber = 15;
		mAnimationRow = 1;
		mCurrentFrame = 0;
		break;
	case Soldier::STAY:
		mFramesNumber = 6;
		mAnimationRow = 0;
		mCurrentFrame = 0;
		break;
	case Soldier::DIE:
		mFramesNumber = 6;
		mAnimationRow = 4;
		mCurrentFrame = 0;
		mLoopAnim = false;
		mAnimSpeed = 10;
		break;
	default:
		break;
	}

	switch (mAction)
	{
	case Soldier::JUMP:
		mFramesNumber = 7;
		mAnimationRow = 3;
		mCurrentFrame = 0;
		mLoopAnim = false;
		break;
	case Soldier::FALL:
		mFramesNumber = 7;
		mAnimationRow = 3;
		mCurrentFrame = 6;
		mAnimFinished = true;
		mLoopAnim = false;
		break;
	case Soldier::NONE:
		break;
	case Soldier::SHOOT:
		mFramesNumber = 6;
		mAnimationRow = 2;
		mCurrentFrame = 0;
		mLoopAnim = false;
		mAnimFinished = false;
		break;
	default:
		break;
	}

	switch (mDir)
	{
	case Soldier::LEFT:
		mFlip = SDL_FLIP_HORIZONTAL;
		break;
	case Soldier::RIGHT:
		mFlip = SDL_FLIP_NONE;
		break;
	default:
		break;
	}
}

std::pair<int, int> Soldier::xyToBord(float x, float y)
{
	//change real coordinates to board coord

	y -= mBoard->getDY();
	int by = static_cast<int>(floor(y/mBoard->getBrickH()));
	int bx = static_cast<int>(floor(x/mBoard->getBrickW()));

	return std::pair<int, int>(bx, by);
}
