#include "BulletHandler.h"
#include "Game.h"


BulletHandler::BulletHandler() : 
	BULLET_SPEED(200, 0)
{
	mBullets.reserve(50);

	mLoopAnim = false;
	mAnimFinished = true;
	mAnimationRow = 5;
	mCurrentFrame = 2;
}


BulletHandler::~BulletHandler()
{
}

void BulletHandler::draw()
{
	SDL_Rect destRect{ 0, 0, sBulletWidth, sBulletHeight };

	for (auto& b : mBullets)
	{
		destRect.x = static_cast<int>(b.pos.getX());
		destRect.y = static_cast<int>(b.pos.getY());


		Game::Instance()->getTextureMenager()->drawFrame(mTextureID, destRect,
			mCurrentFrame, mAnimationRow, Game::Instance()->getRenderer(), b.flip);
	}
}

void BulletHandler::update()
{
	//update bullets positions
	for (auto& b : mBullets)
	{
		b.pos += BULLET_SPEED * 
			(b.flip == SDL_FLIP_NONE ? 1.0f : -1.0f) * Game::Instance()->getDT();
	}

	//check for collisions
	for (auto& b : mBullets)
	{
		//x axis window collison
		if (b.pos.getX() < 0)
			b.remove = true;
		else if (Game::Instance()->getGameWidth() < (b.pos.getX() + sBulletWidth))
		{
			b.remove = true;
		}
		//y axis window collison
		else if(Game::Instance()->getGameHight() < (b.pos.getY() + sBulletHeight))
		{
			b.remove = true;
		}
		else if (b.pos.getY() < 0)
		{
			b.remove = true;
		}
		////board collision
		else
		{
			std::pair<int, int>  boardPos1 = 
				xyToBord(b.pos.getX(), b.pos.getY());
			std::pair<int, int> boardPos2 = 
				xyToBord(b.pos.getX() + sBulletWidth, b.pos.getY());
			std::pair<int, int> boardPos3 =
				xyToBord(b.pos.getX(), b.pos.getY() + sBulletHeight);
			std::pair<int, int> boardPos4 =
				xyToBord(b.pos.getX() + sBulletWidth, b.pos.getY() + sBulletHeight);

			if (mBoard->getBrick(boardPos1.first, boardPos1.second) ||
				mBoard->getBrick(boardPos2.first, boardPos2.second) ||
				mBoard->getBrick(boardPos3.first, boardPos3.second) ||
				mBoard->getBrick(boardPos4.first, boardPos4.second))
			{
				b.remove = true;
			}

		}
	}

	//remove old bullets that have hit sth
	auto eraser = mBullets.begin();
	while (eraser != mBullets.end())
	{
		if (eraser->remove)
		{
			eraser = mBullets.erase(eraser);
		}
		else
			++eraser;
	}
}

void BulletHandler::addBullet(float x, float y, SDL_RendererFlip flip)
{
	mBullets.push_back(Bullet(x, y, flip));
}

void BulletHandler::getDataToSend(char * tab, int & size)
{
	size += 1 + 4 * mBullets.size();
	*tab = static_cast<char>(mBullets.size());
	++tab;
	int *data = (int*)tab;

	//represent every bullet position as one integer
	//where first bit represents flip
	//following 15 bits represent y coordinate 
	//and last 16 bits represent x coordinate
	for (auto& b : mBullets)
	{
		int x = static_cast<int>(b.pos.getX());
		int y = static_cast<int>(b.pos.getY());

		x &= 0x0000FFFF;
		y <<= 16;
		y = y & 0xFFFF0000;
		x =  x|y;

		if (b.flip != SDL_FLIP_NONE)
		{
			x = x | 0x80000000;
		}

		*data = x;
		++data;
	}
}

bool BulletHandler::playerColision(float x, float y, float w, float h)
{
	for (auto& b : mBullets)
	{
		if ( (x < (b.pos.getX() + sBulletWidth)) && 
			(b.pos.getX() < (x + w)) && 
			(y < (b.pos.getY()+sBulletHeight)) &&
			(b.pos.getY() < (y+h)) )
		{
			b.remove = true;
			return true;
		}
	}


	return false;
}

std::pair<int, int> BulletHandler::xyToBord(float x, float y)
{
	//change real coordinates to board coord

	y -= mBoard->getDY();
	int by = static_cast<int>(floor(y / mBoard->getBrickH()));
	int bx = static_cast<int>(floor(x / mBoard->getBrickW()));

	return std::pair<int, int>(bx, by);
}
