#include "TextTextureManager.h"
#include "Texture.h"
#include <iostream>


TextTextureManager::TextTextureManager() :
	mFont(nullptr)
{
}


TextTextureManager::~TextTextureManager()
{
}

//load font for creating text textures
bool TextTextureManager::loadFont(std::string fontFile, int size)
{
	clear();
	mFont = std::shared_ptr<TTF_Font>(TTF_OpenFont(fontFile.c_str(), size),
		[](TTF_Font* font) { TTF_CloseFont(font); });
	if (mFont)
	{
		std::cout << "Failed to load lazy font! SDL_ttf Error: " <<
			(int)TTF_GetError() << "\n";
		mFont = nullptr;
		return false;
	}
	return true;
}

void TextTextureManager::clear()
{
}

std::unique_ptr<Texture> TextTextureManager::createTextTexture(
	std::string textureText, SDL_Color textColor,
	SDL_Renderer* pRenderer)
{
	std::unique_ptr<Texture> textTexture = std::make_unique<Texture>();
	//Render text surface
	SDL_Surface* textSurface = TTF_RenderText_Solid(mFont.get(), textureText.c_str(), textColor);
	if (textSurface == NULL)
	{
		std::cout << "Unable to render text surface! SDL_ttf Error:" <<
			TTF_GetError() << "\n";
		return false;
	}
	else
	{
		//Create texture from surface pixels
		SDL_Texture* pTexture = SDL_CreateTextureFromSurface(pRenderer, textSurface);
		if (pTexture == NULL)
		{
			std::cout << "Unable to create texture from rendered text! SDL Error: " <<
				SDL_GetError() << "\n";
			SDL_FreeSurface(textSurface);
		}
		else
		{
			std::cout << "TEXT texture loaded \t" << textureText << "\n";

			textTexture->load(pTexture,
				textSurface->w, textSurface->h);
			SDL_FreeSurface(textSurface);
			return textTexture;
		}

		//Get rid of old surface
		SDL_FreeSurface(textSurface);
	}

	//Return failure
	return nullptr;
}