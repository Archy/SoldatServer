#pragma once
#include <string>

class LoaderParams
{
public:
	//for button-like objects
	LoaderParams(int x, int y, int width, int height, const std::string
		textureID, const std::string text = std::string(), int callbackID = 0) :
		mX(x),
		mY(y),
		mWidth(width),
		mHeight(height),
		mTextureID(textureID),
		mText(text),
		mCallbackID(callbackID),
		mFramesNr(0),
		mAnimSpeed(0)
	{
	}

	//for animated objects
	LoaderParams(int x, int y, int width, int height, const std::string
		textureID, int framesNr, int animSpeed) :
		mX(x),
		mY(y),
		mWidth(width),
		mHeight(height),
		mTextureID(textureID),
		mFramesNr(framesNr),
		mAnimSpeed(animSpeed),
		mText(std::string()),
		mCallbackID(0)
	{
	}

	//get functions
	int getX() const { return mX; }
	int getY() const { return mY; }
	int getWidth() const { return mWidth; }
	int getHeight() const { return mHeight; }
	std::string getTextureID() const { return mTextureID; }
	std::string getText() const { return mText; }
	int getCallbackID() const { return mCallbackID; }
	int getFramesNr() const { return mFramesNr; }
	int getAnimSpeed() const { return mAnimSpeed; }

private:
	//basic position
	int mX;
	int mY;

	//dimensions
	int mWidth;
	int mHeight;

	std::string mTextureID;

	//for button-like objects
	std::string mText;
	int mCallbackID;

	//for animated objects
	int mFramesNr;
	int mAnimSpeed;
};