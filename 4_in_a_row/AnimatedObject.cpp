#include "AnimatedObject.h"
#include <SDL.h>
#include "Game.h"
#include "LoaderParams.h"

AnimatedObject::AnimatedObject() : 
	mCurrentFrame(0), mFlip(SDL_FLIP_NONE), mLoopAnim(true), mFrameTime(0),
	mAnimFinished(false)
{
	mAnimationRow = 0;
}


AnimatedObject::~AnimatedObject()
{
}


void AnimatedObject::load(std::unique_ptr<LoaderParams> const &pParams)
{
	//load additional data for animation handling
	mFramesNumber = pParams->getFramesNr();
	mAnimSpeed = pParams->getAnimSpeed();
	mCurrentFrame = 0;

	GameObject::load(pParams);
}


void AnimatedObject::draw()
{
	SDL_Rect destRect{ static_cast<int>(mPosition.getX()), 
		static_cast<int>(mPosition.getY()),
		mWidth, mHeight };
	
	Game::Instance()->getTextureMenager()->drawFrame(mTextureID, destRect,
		mCurrentFrame, mAnimationRow, Game::Instance()->getRenderer(), mFlip);
}

void AnimatedObject::update()
{
	//change frame over time	
	mFrameTime += Game::Instance()->getDT();
	if (mFrameTime > (1.0 / mAnimSpeed) && !mAnimFinished)
	{
		mCurrentFrame++;
		if (mCurrentFrame == mFramesNumber)
		{
			if (mLoopAnim)
				mCurrentFrame = 0;
			else
			{
				mAnimFinished = true;
				mCurrentFrame = mFramesNumber - 1;
			}
		}

		mFrameTime = 0;
	}
}


void AnimatedObject::clean()
{
}