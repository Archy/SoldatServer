#include "Vector2D.h"


Vector2D Vector2D::operator+(const Vector2D& v2) const
{
	return Vector2D(mX + v2.mX, mY + v2.mY);
}


Vector2D& Vector2D::operator+=(const Vector2D& v2)
{
	mX += v2.mX;
	mY += v2.mY;
	return *this;
}



Vector2D Vector2D::operator*(float scalar) const
{
	return Vector2D(mX * scalar, mY * scalar);
}


Vector2D& Vector2D::operator*=(float scalar)
{
	mX *= scalar;
	mY *= scalar;
	return *this;
}



Vector2D Vector2D::operator-(const Vector2D& v2) const
{
	return Vector2D(mX - v2.mX, mY - v2.mY);
}


Vector2D Vector2D::operator-=(const Vector2D& v2)
{
	mX -= v2.mX;
	mY -= v2.mY;
	return *this;
}



Vector2D Vector2D::operator/(float scalar) const
{
	return Vector2D(mX / scalar, mY / scalar);
}


Vector2D& Vector2D::operator/=(float scalar)
{
	mX /= scalar;
	mY /= scalar;
	return *this;
}



void Vector2D::normalize()
{
	float l = length();
	if (l > 0) // we never want to attempt to divide by 0
	{
		(*this) *= 1 / l;
	}
}