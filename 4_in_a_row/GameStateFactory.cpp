#include "GameStateFactory.h"
#include "GameStateCreator.h"
#include <iostream>


GameStateFactory::GameStateFactory()
{
}


GameStateFactory::~GameStateFactory()
{
}


void GameStateFactory::registerState(int id, GameStateCreator* stateCreator)
{
	auto it= mStatesCreator.find(id);
	// if the type is already registered, do nothing
	if (it != mStatesCreator.end())
	{
		delete stateCreator;
		std::cout << "State already registered: " << id << "\n";
	}
	else
	{
		mStatesCreator[id] = stateCreator;
		std::cout << "State registered: " << id << "\n";
	}
}


GameState* GameStateFactory::createState(const int id)
{
	auto it = mStatesCreator.find(id);
	// if the type does exists
	if (it != mStatesCreator.end())
	{
		std::cout << "State created: " << id << "\n";
		return mStatesCreator[id]->createGameObject(id);
	}
	//type does not exists
	else
	{
		std::cout << "No such state to create: " << id << "\n";
		return nullptr;
	}
}


void GameStateFactory::clean()
{
	for (auto it : mStatesCreator)
	{
		delete it.second;
		it.second = nullptr;
	}

	mStatesCreator.clear();
}