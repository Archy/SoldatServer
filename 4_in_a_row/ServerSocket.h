#pragma once
#include "Socket.h"
#include <string>

class ServerSocket : public Socket
{
public:
	ServerSocket();
	~ServerSocket();

	bool init(int port);
	//send data of given size to IP and port
	bool sendData(const char *data, int &size, std::string remoteIP, const USHORT port);

private:
	//set server additional data
	bool createServer();

private:
	int mPort;
};

