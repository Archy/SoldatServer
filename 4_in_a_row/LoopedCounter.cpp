#include "LoopedCounter.h"



LoopedCounter::LoopedCounter() : 
	mx(0)
{
}


LoopedCounter::~LoopedCounter()
{
}

bool LoopedCounter::operator<(const unsigned char& other)
{
	if (mx<other)
		return true;
	else if (235<mx && other<20)
		return true;
	else
		return false;
}

unsigned char LoopedCounter::getNext()
{
	++mx;
	return mx;
}

void LoopedCounter::setValue(unsigned char x)
{
	mx = x;
}
