#include "Enemy.h"
#include <SDL.h>
#include "Game.h"
#include "PlayState.h"

#include "GameLog.h"

Enemy::Enemy()
{
}


Enemy::~Enemy()
{
}


void Enemy::update()
{
	this->m_ticksActual = SDL_GetTicks() - this->m_ticksPrev;

	if (this->m_pState->getActualPlayer() != this->m_pID)
	{
		this->m_ticksPrev = this->m_ticksActual;
		return;
	}

	if (this->m_coinFalling == false)
	{
		int column = this->m_pIntelligence.MinMax(this->m_pBoard->getBoard(),
			Board::field::p2, Board::field::p1);
		
		GameLog::Instance() << "Enemy stars throwing at " << column << "\n";

		SDL_Rect range = Game::Instance()->getPlayRect();
		this->m_position.setX(static_cast<float>(
			column * (range.w / 7) + range.x));
		this->initFalling();
	}

	if (!this->m_coinFalling)
	{
		this->horizontalMove();
	}
	//coin is falling
	else
	{
		this->fallingMove();
	}

	this->m_ticksPrev = this->m_ticksActual;
}