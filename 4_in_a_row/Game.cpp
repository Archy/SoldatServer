#include "Game.h"
#include <iostream>

std::shared_ptr<Game> Game::sInstance;
bool Game::sCreatedInstance = false;

Game::Game() :
	mRunning(false),
	mMySDL()
{
}

Game::~Game()
{
}


void Game::clean()
{
	mTextureMenager->clean();
	mMySDL->clean();
}

//get game instance
std::shared_ptr<Game> Game::Instance()
{
	if (!sCreatedInstance) {
		sCreatedInstance = true;
		sInstance = std::make_shared<Game>();
	}

	return Game::sInstance;
}

void Game::deleteInstance()
{

}


bool Game::init(const char* title, int xpos, int ypos, 
	int width, int height, bool fullscreen)
{
	bool initSuccess = false;

	// store the game width and height
	mGameWidth = width;
	mGameHeight = height;

	//set up SDL
	mMySDL = std::make_shared<MySDL>();
	initSuccess = mMySDL->init(title, xpos, ypos, width,
		height, fullscreen);
	if (initSuccess)
	{
		//game is running
		mRunning = true;

		//init textureManager
		mTextureMenager = std::make_shared<TextureMenager>();
		mTextureMenager->init();

		//init playstate
		mState = std::make_shared<Soldat>();
		mState->init();

		//init timer
		mTimer = std::make_shared<Timer>();
		mTimer->start();
	}
	return initSuccess;
}


void  Game::render()
{
	//clear rendering target
	SDL_RenderClear(mMySDL->getRenderer().get());
	
	//draw objects to render target
	mState->draw();

	//show render target on screen
	SDL_RenderPresent(mMySDL->getRenderer().get());
}

void  Game::update()
{
	//update game state
	mTimer->update();
	mState->update();
}

void  Game::handleEvents()
{
	//handle window close
	SDL_Event event;
	while (SDL_PollEvent(&event))
	{
		switch (event.type)
		{
		case SDL_QUIT:
			Game::Instance()->quit();
			break;
		}
	}
}


SDL_Rect Game::getPlayRect()
{
	SDL_Rect playRect;
	playRect.x = 110;
	playRect.y = 60;
	playRect.w = 420;
	playRect.h = 420;

	return playRect;
}