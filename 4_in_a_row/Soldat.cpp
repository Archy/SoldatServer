#include "Soldat.h"
#include "Game.h"

#include "LoaderParams.h"
#include "Board.h"


Soldat::Soldat() :
	mStared(false), mPacketsCounter(4), mFinished(false)
{
}

Soldat::~Soldat()
{
}

void Soldat::init()
{
	// TEXTURES
	if (Game::Instance()->getTextureMenager()->loadTexture(
		"assets/brick.png", "brick",
		Game::Instance()->getRenderer()))
	{
		mTextureIDList.push_back("brick"); // push into list
	}

	SDL_Color soldierTransparent{ 184, 72, 184, 255 };
	if (Game::Instance()->getTextureMenager()->loadSpriteShit(
		"assets/soldier.png", "soldier", Game::Instance()->getRenderer(),
		&soldierTransparent, "assets/meta.txt"))
	{
		mTextureIDList.push_back("soldier"); // push into list
	}

	// OBJECTS
	std::shared_ptr<Board> board = std::make_shared<Board>();
	board->load(std::unique_ptr<LoaderParams>(new LoaderParams(0, 5,
		Game::Instance()->getGameWidth(), Game::Instance()->getGameHight() - 5, "brick")));
	mStateObjects.push_back(board);

	std::shared_ptr<BulletHandler> bullets = std::make_shared<BulletHandler>();
	bullets->load(std::unique_ptr<LoaderParams>(new LoaderParams(0,
		0, 16, 8, "soldier", 1, 12)));
	bullets->setBoard(board);
	mStateObjects.push_back(bullets);
	mBullets = bullets;

	int soldierPositions[8] = { 40, 5, Game::Instance()->getGameWidth() - 80, 5, 200,  };
	//soldiers
	for (int i = 0; i < SOLDIER_NR; ++i)
	{
		std::shared_ptr<Soldier> s = std::make_shared<Soldier>(i);
		s->load(std::unique_ptr<LoaderParams>(new LoaderParams(soldierPositions[2 * i],
			soldierPositions[2 * i + 1], 40, 50, "soldier", 6, 12)));
		s->setBoard(board);
		s->setBulletHandler(bullets);
		mStateObjects.push_back(s);
		mSoldiers.push_back(s);
	}


//create server
	char localIP[16];
	mServer = std::make_unique<ServerSocket>();
	mServer->init(44666);
	mServer->getLocalIP(localIP);
	std::cout << "\n\tServer IP is: " << localIP << "\n\n";
}

void Soldat::draw()
{
	for (auto& it : mStateObjects)
	{
		it->draw();
	}/**/
}

void Soldat::update()
{
	//first communcate with clients
	communicate();

	//update game
	if (mStared && !mFinished)
	{
		for (auto& it : mStateObjects)
		{
			it->update();
		}
	}

	//count alive players
	int alive = 0;
	for (auto& soldier : mSoldiers)
	{
		if (soldier->isAlive())
		{
			++alive;
		}
	}
	if (alive <= 1)
	{
		mFinished = true;
	}

	if (mFinished)
		return;
	//soldiers - bullets collisions
	for (auto& soldier : mSoldiers)
	{
		if (soldier->isAlive())
		{
			if (mBullets->playerColision(soldier->getX() + 7, soldier->getY() + 2,
				soldier->getWidth() - 14, soldier->getHeight() - 4))
			{
				soldier->hit();
				std::cout << "PLAYER " << (int)soldier->getNr() << " hit\n";
			}
		}
	}
}

void Soldat::communicate()
{
	const int BUFSIZE = 512;
	const int ADITIONAL_PACKET_DATA = 1;
	char buff[BUFSIZE];
	
	int reveivedSize = 0;
	char senderIP[16];
	USHORT senderPort;

	bool answer = true;

	memset(buff, 0, BUFSIZE);
	reveivedSize = BUFSIZE;
	//read all received data
	while (mServer->readData(buff, reveivedSize, senderIP, senderPort))
	{
		std::pair<std::string, USHORT> player(std::string(senderIP), senderPort);

		if (reveivedSize > 0)
		{
			//if this user has already connected to sever update his soldiers state
			if (mPlayers.find(player)
				!= mPlayers.end())
			{
				//check for outdate packets
				//and dont update when game is finished
				unsigned char receivedNr = reinterpret_cast<unsigned char&>(buff[0]);
				if (mPacketsCounter[mPlayers[player]].lastReceived < receivedNr &&
					!mFinished) 
				{
					setSoldierState(buff[ADITIONAL_PACKET_DATA], mPlayers[player]);
					mPacketsCounter[mPlayers[player]].lastReceived.setValue(receivedNr);
				}
			}
			else
			{
				if (mPlayers.size() < SOLDIER_NR)	//if user can connect to server
				{
					int newNr = mPlayers.size();
					std::cout << "NEW PLAYER: " << std::string(senderIP) << " - " << senderPort <<
						"\t" << newNr << "\n";
					mPlayers[player] = newNr;
				}
				else
				{
					answer = false;	//dont reply to this user 
					std::cout << "ALREADY FULL\n";
				}
			}

			mStared = mPlayers.size() == SOLDIER_NR;

		}
		
		if (answer)
		{
			//send data back to sender	
			const int SOLDIER_DATA = 9;
		//prepare data to send to player
			//packet nr
			char packetNr = mPacketsCounter[mPlayers[player]].lastSend.getNext();
			buff[0] = reinterpret_cast<unsigned char&>(packetNr);
			//actual data
			for (int i = 0; i < SOLDIER_NR; ++i)
			{
				buff[SOLDIER_DATA*i + ADITIONAL_PACKET_DATA] = mSoldiers[i]->getStateToSend();
				float *pos = (float*)&buff[1 + SOLDIER_DATA*i + ADITIONAL_PACKET_DATA];
				*pos = mSoldiers[i]->getX();
				pos = (float*)&buff[5 + SOLDIER_DATA*i + ADITIONAL_PACKET_DATA];
				*pos = mSoldiers[i]->getY();
			}
			// (2xfloat + char) x soldiers_nr  +  player_info
			int size = 9 * SOLDIER_NR + ADITIONAL_PACKET_DATA + 1; 
			//player info: nr ammo lives
			buff[size - 1] = mSoldiers[mPlayers[player]]->getPlayerInfo(); 

			//set bullets data
			mBullets->getDataToSend(&(buff[size]), size);
			
			if (!mStared)
			{
				size = 1;
				buff[0] = 0;
			}
			else if (mFinished)
			{
				if (mSoldiers[mPlayers[player]]->isAlive())
				{
					buff[9 * SOLDIER_NR + ADITIONAL_PACKET_DATA] = 7;
				}
				else
				{
					buff[9 * SOLDIER_NR + ADITIONAL_PACKET_DATA] = 6;
				}	
			}
			if (!mServer->sendData(buff, size, senderIP, senderPort))
			{
				std::cout << "FAILED TO SEND DATA";
			}
		}		
	}
}

void Soldat::setSoldierState(char a, int nr)
{
	//	xx		x		xx			xx			x
	//	nr		dir		state		action		(free)

	a &= 0x3F;	//reset nr bits

	char dir = (a & 0x20) >> 5;
	char state = (a & 0x18) >> 3;
	char action = (a & 0x06) >> 1;

	Soldier::SoldierDir soldierDir = (dir==1 ?
		Soldier::SoldierDir::RIGHT : Soldier::SoldierDir::LEFT);

	Soldier::SoldierState soldierState;
	switch (state)
	{
	case 0:
		soldierState = Soldier::SoldierState::STAY;
		break;
	case 1:
		soldierState = Soldier::SoldierState::MOVE;
		break;
	case 3:
		soldierState = Soldier::SoldierState::DIE;
		break;
	}
	
	Soldier::SoldierAction soldierAction;
	switch (action)
	{
	case 0:
		soldierAction = Soldier::SoldierAction::NONE;
		break;
	case 1:
		soldierAction = Soldier::SoldierAction::JUMP;
		break;
	case 2:
		soldierAction = Soldier::SoldierAction::FALL;
		break;
	case 3:
		soldierAction = Soldier::SoldierAction::SHOOT;
		break;
	}

	mSoldiers[nr]->setState(soldierState, soldierAction, soldierDir);
}

