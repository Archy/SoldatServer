#pragma once
#include <memory>
#include <vector>
#include <string>
#include <map>
#include "GameObject.h"
#include "Soldier.h"
#include "ServerSocket.h"
#include "LoopedCounter.h"
#include "BulletHandler.h"

class Soldat
{
public:
	Soldat();
	~Soldat();

	void init();
	void draw();
	void update();

private:
	void communicate();
	void setSoldierState(char a, int nr);

private:
	struct PlayerPackets
	{
		LoopedCounter lastSend;
		LoopedCounter lastReceived;
	};

private:
	//used to remove them when exiting the state
	//ids of textures used by GameState instance
	std::vector<std::string> mTextureIDList;

	//vector of object used in state
	std::vector<std::shared_ptr<GameObject>> mStateObjects;

	std::vector<std::shared_ptr<Soldier>> mSoldiers;
	std::map<std::pair<std::string, USHORT>, int> mPlayers;

	std::unique_ptr<ServerSocket> mServer;

	//set by when exactly 4 players connect
	bool mStared;

	//if game has been finished
	bool mFinished;

	//players packets numerations
	std::vector<PlayerPackets> mPacketsCounter;

	//bullet handler
	std::shared_ptr<BulletHandler> mBullets;

	static const int SOLDIER_NR = 2;
};

