#include "StateMachine.h"
#include "GameState.h"
#include "GameStateFactory.h"
#include "Game.h"
#include <SDL.h>
#include <iostream>

//states
#include "MainMenuState.h"
#include "PauseState.h"
#include "GameOverState.h"
#include "PlayState.h"



StateMachine::StateMachine()
{
}

StateMachine::~StateMachine()
{
}


void StateMachine::init()
{
	mStatesFactory = 
		std::unique_ptr<GameStateFactory>(new GameStateFactory);

	//create background textrure
	Game::Instance()->getTextureMenager()->createBackgroundTexture("aphaGREY",
		SDL_Color{ 128, 128, 128, 255 }, Game::Instance()->getRenderer(),
		SDL_BLENDMODE_BLEND, 191);

	//TODO register states
	mStatesFactory->registerState(StateMachine::MainMenuState, 
		new MainMenuStateCreator());
	mStatesFactory->registerState(StateMachine::GameEndState,
		new GameOVerStateCreator());
	mStatesFactory->registerState(StateMachine::PlayState,
		new PlayStateCreator());
}

void StateMachine::clean()
{
	for (auto& it : mGameState)
	{
		it->clear();
		delete it;
		it = nullptr;
	}
	mGameState.clear();
	//emptyVector(mGameState);

	removeOldStates();

	std::cout << "StateMachine cleaned successfully\n";
}

void StateMachine::removeOldStates()
{
	if (mOldGameStates.empty())
		return; 

	for (auto& it : mOldGameStates)
	{
		it->clear();
		delete it;
		it = nullptr;
	}
	mOldGameStates.clear();
}


void StateMachine::update()
{
	removeOldStates();

	if (!mGameState.empty())
	{
		mGameState.back()->update();
	}
	else
	{
		std::cout << "Attempt to update when " <<
			"StateMachine is empty\n";
	}
}

void StateMachine::render()
{
	if (!mGameState.empty())
	{
		mGameState.back()->render();
	}
	else
	{
		std::cout << "Attempt to render when " <<
								"StateMachine is empty\n";
	}
}


//add new state
void StateMachine::addState(const StateMachine::GameStatesIDs id)
{
	//attempt to add new state
	GameState* stateToAdd = nullptr;
	int statePos = 0;
	findState(&stateToAdd, &statePos, id);

	//state already exists
	if (stateToAdd != nullptr)
	{
		stateToAdd->clear();
		mGameState.erase(mOldGameStates.begin() + statePos);
		mGameState.push_back(stateToAdd);
		stateToAdd->EnterState();

		std::cout << "Game state reseted: " << id
			<< '\n';
	}
	//state has to be added
	else
	{
		stateToAdd = mStatesFactory->createState(id);
		if (stateToAdd == nullptr)
		{
			std::cout << "Game state not added: " << id
				<< "State Factory returns nullptr" << '\n';
			return;
		}
		mGameState.push_back(stateToAdd);
		stateToAdd->EnterState();

		std::cout << "Game state added: " << id << '\n';
		
	}
} 

//remove state by id
void StateMachine::removeState(const StateMachine::GameStatesIDs id)
{
	GameState* stateToRemove = nullptr;
	int statePos = 0;
	findState(&stateToRemove, &statePos, id);

	if (stateToRemove == nullptr)
	{
		std::cout << "Game state not removed" <<
			": no state" << id << '\n';
	}
	else
	{
		stateToRemove->ExitState();
		stateToRemove->clearData();
		mOldGameStates.push_back(stateToRemove);
		mGameState.erase(mOldGameStates.begin() + statePos);

		std::cout << "Game state removed" << id
			<< '\n';
	}
}

//remove state is currently being used
void StateMachine::removeCurrentState()
{
	//attempt to remove previous state
	if (!mGameState.empty())
	{
		mGameState.back()->ExitState();
		mGameState.back()->clearData();
		mOldGameStates.push_back(mGameState.back());
		mGameState.pop_back();
		//previous state removed
		int oldID = mOldGameStates.back()->getStateID();
		std::cout << "Current State removed: " << oldID << '\n';
	}
	else
	{
		std::cout << "Current game state not removed" <<
			": state machine empty\n";
	}
}

//change state that is currently used
void StateMachine::changeState(const StateMachine::GameStatesIDs id)
{
	std::cout << "change state called\n";
	
	removeCurrentState();
	addState(id);	

	
}


void StateMachine::findState(GameState** stateToRemove, int* statePos, int id)
{
	for (auto& it : mGameState)
	{
		if (it->getStateID() == id)
		{
			*stateToRemove = it;
			break;
		}
		++(*statePos);
	}
}