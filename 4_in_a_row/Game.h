#pragma once
#include <memory>
#include "MySDL.h"
#include "TextureMenager.h"
#include "Timer.h"
#include "Soldat.h"

class Game final
{
public:
	Game();
	~Game();

	static std::shared_ptr<Game> Instance();
	static void deleteInstance();

	bool init(const char* title, int xpos, int ypos, 
		int width, int height, bool fullscreen);

	void render();
	void update();
	void handleEvents();
	
	void clean();

	//get functions
	bool isRunning() { return mRunning; }
	std::shared_ptr<SDL_Renderer> getRenderer(){ return mMySDL->getRenderer(); }
	std::shared_ptr<TextureMenager> getTextureMenager(){ return mTextureMenager; }

	float getDT() { return mTimer->getDT(); }

	int getGameWidth() const { return mGameWidth; }
	int getGameHight() const { return mGameHeight; }

	SDL_Rect getPlayRect();

	//ending main game loop
	void quit() { mRunning = false; }

private:
	//singleton instance
	static std::shared_ptr<Game> sInstance;
	static bool sCreatedInstance;

	//for SDL handling
	std::shared_ptr<MySDL> mMySDL;
	
	//for texture managing
	std::shared_ptr<TextureMenager> mTextureMenager;

	//game time
	std::shared_ptr<Timer> mTimer;

	//actual play state
	std::shared_ptr<Soldat> mState;

	//if game is running
	bool mRunning;

	//game dimensions
	int mGameWidth;
	int mGameHeight;
};